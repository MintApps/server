/**
 * Web server used to synchronize messages in real time between multiple participants in a game
 * @author Thomas Kippenberg
 * @module server-websockets
*/

import consts from './consts.js'
import * as helpers from './helpers.js'
import { getConnection } from './database.js'
import { debug } from 'console'
import config from './config.json' with { type: 'json' }
import * as serverRooms from './server-rooms.js'

// web socket server (set from run method)
let wss

// synchronize critical asynchronous parts
let mutex = Promise.resolve()

/**
 * Send message to room owner
 * @param {WebSocket} ws - web socket
 * @param {String} message - message text
 */
function sendMessageToOwner (ws, message) {
  if (ws.mintapps?.room) {
    wss.clients.forEach(wsClient => {
      if (wsClient.mintapps?.member === ws.mintapps.owner) {
        wsClient.send(message)
      }
    })
  }
}

/**
 * Send message to all room members
 * @param {String} roomId - id of room
 * @param {String} message - message text
 */
export function sendMessageToRoom (roomId, message) {
  wss.clients.forEach(wsClient => {
    if (wsClient.mintapps?.room === roomId) {
      wsClient.send(message)
    }
  })
}

/**
 * Start websocket server
 * @param {Object} _wss - web socket server
 */
export function init (_wss) {
  wss = _wss
  // eslint-disable-next-line no-unused-vars
  wss.on('connection', (ws, request, client) => {
    ws.isAlive = true
    // response to pint message
    ws.on('pong', () => {
      ws.isAlive = true
    })
    // websocket closed
    ws.on('close', () => {
      if (ws.mintapps) {
        sendMessageToOwner(ws, JSON.stringify({ action: 'delete-member', member: ws.mintapps.member }))
        debug(`user ${ws.mintapps.member} left room ${ws.mintapps.room}`)
        ws.mintapps = null
      }
    })
    // websocket message
    ws.on('message', async (encodedData) => {
      // try to parse data and exit if invalid JSON
      let data
      try {
        data = JSON.parse(encodedData)
      } catch {
        return ws.send(JSON.stringify({ error: 'message-invalid-json' }))
      }
      await handleMessage(ws, data)
    })
  })

  // periodically ping websockets to keep connection in reverse-proxy alive
  setInterval(() => {
    debug(`Web socket number: ${wss.clients.size}`)
    wss.clients.forEach(ws => {
      if (!ws.isAlive) {
        ws.terminate() // will trigger 'close' event
      } else {
        ws.isAlive = false
        ws.ping()
      }
    })
  }, consts.WS_PING_INTERVAL)
}

/**
 * Internal function, handle websocket messages using the mutex pattern
 * Process messages one by one (to avoid race conditions due to asynchronous database operations)
 * @param {WebSocket} ws - websocket of request
 * @param {Object} data - payload of request
 * @see {@link https://www.nodejsdesignpatterns.com/blog/node-js-race-conditions/}
 */
async function handleMessage (ws, data) {
  mutex = mutex.then(async () => {
    try {
      // join room request
      if (data.action === 'join-room') {
        if (!data.room || !data.room.match(consts.ROOM_ID_REGEX)) {
          ws.send(JSON.stringify({ error: 'invalid-room-id' }))
        } else if (data.user && !serverRooms.checkToken(data.room, data.user, data.token)) { // check user & token if given in request
          ws.send(JSON.stringify({ error: 'token-invalid' }))
        } else {
          const room = await getRoom(data.room)
          // check member limit, not perfectly thread save...
          let memberNumber = 0
          wss.clients.forEach(wsClient => {
            if (wsClient.mintapps?.room === room.id) memberNumber++
          })
          if (memberNumber >= room.memberLimit) {
            ws.send(JSON.stringify({ error: 'room-member-limit' }))
          } else {
            // create and add member
            const memberId = helpers.getTextParam(data.user, consts.USER_ID_REGEX, helpers.getRandomToken(12))
            const newMember = memberId !== data.user
            const token = helpers.getTextParam(data.token, consts.ROOM_TOKEN_REGEX, serverRooms.createToken(room.id, memberId))
            const pos = Number.isInteger(data.pos) ? data.pos : 0
            await touchRoom(room.id)
            ws.mintapps = { room: room.id, member: memberId, owner: room.owner }
            ws.send(JSON.stringify({ action: 'set-room', room: room.id, user: memberId, token, owner: room.owner, name: room.name, lifetime: Number(room.lifetime) }))
            // info to room owner for new members
            sendMessageToOwner(ws, JSON.stringify({ action: 'add-member', member: memberId, newMember }))
            // send old messages from history
            const allMessages = await getMessages(room.id, pos)
            const messages = []
            for (const entry of allMessages) {
              if (entry.dst === '*' || entry.dst === memberId) {
                messages.push({ from: entry.src, to: entry.dst, pos: entry.id, content: JSON.parse(entry.content) })
              }
            }
            if (messages.length > 0) {
              ws.send(JSON.stringify({ action: 'room-messages', messages }))
            }
            if (newMember) debug(`user ${memberId} joined room ${room.id}`)
            else debug(`user ${memberId} reentered room ${room.id}`)
          }
        }
      } else if (data.action === 'send-room-message') {
        // send message to room members
        const content = data.content
        if (!data.to || data.to === '') data.to = '*'
        if (JSON.stringify(content).length > consts.MESSAGE_MAX_LENGTH) {
          ws.send(JSON.stringify({ error: 'message-invalid-length' }))
        } else if (!ws.mintapps?.room?.match(consts.ROOM_ID_REGEX)) {
          ws.send(JSON.stringify({ error: 'invalid-room-id' }))
        } else if (!ws.mintapps?.member?.match(consts.USER_ID_REGEX)) {
          ws.send(JSON.stringify({ error: 'invalid-message-from' }))
        } else if (!data.to === '*' && !data.to?.match(consts.USER_ID_REGEX)) {
          ws.send(JSON.stringify({ error: 'invalid-message-to' }))
        } else {
          const pos = await addMessage(ws.mintapps.room, { from: ws.mintapps.member, to: data.to, content })
          wss.clients.forEach(wsClient => {
            if (wsClient.mintapps?.room === ws.mintapps.room) {
              if (data.to === '*' || data.to === wsClient.mintapps.member) {
                wsClient.send(JSON.stringify({ action: 'room-message', from: ws.mintapps.member, to: data.to, content, pos }))
              }
            }
          })
        }
      } else if (data.action === 'leave-room' && ws.mintapps?.member) {
        // leave room
        sendMessageToOwner(JSON.stringify({ action: 'delete-member', member: ws.mintapps.member }))
      }
    } catch (err) {
      debug(err.message)
      ws.send(JSON.stringify({ error: err.message }))
    }
  })
  return mutex
}

/* internal, database related functions */

/**
 * Update timestamp of room in database
 * @param {String} roomId - id of room
 */
async function touchRoom (roomId) {
  let conn
  try {
    conn = await getConnection()
    const now = Date.now()
    await conn.query('update rooms set changetime = ? where id = ?', [now, roomId])
  } catch (err) {
    debug(err)
    throw new Error('no-db-connection')
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Get room from database
 * @param {String} roomId - id of room
 * @returns {Object} room
 */
async function getRoom (roomId) {
  let conn
  try {
    conn = await getConnection()
    const res = await conn.query('select * from rooms where id = ?', [roomId])
    if (res.length !== 1) throw new Error('room-expired')
    return { id: res[0].id, name: res[0].name, owner: res[0].owner, createTime: res[0].createtime, memberLimit: res[0].memberlimit, lifetime: res[0].lifetime }
  } catch (err) {
    if (err.message === 'room-expired') throw err
    else {
      debug(err)
      throw new Error('no-db-connection')
    }
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Add message to database
 * @param {String} roomId - id of room
 * @param {String} message - message text
 */
async function addMessage (roomId, message) {
  let conn
  try {
    conn = await getConnection()
    const now = Date.now()
    const res0 = await conn.query(
      'select count(id) as count from rooms where id = ?',
      [roomId]
    )
    if (Number(res0[0].count) === 0) throw new Error('room-expired')
    const res1 = await conn.query(
      'select count(id) as count from messages where room = ?',
      [roomId]
    )
    if (Number(res1[0].count) > config.room.historyLimit) throw new Error('room-history-exceeded')
    const res2 = await conn.query(
      'insert into messages (room, dst, src, time, content) values (?, ?, ?, ?, ?)',
      [roomId, message.to, message.from, now, message.content])
    const pos = Number(res2.insertId)
    await conn.query(
      'update rooms set changetime = ? where id = ?',
      [now, roomId]
    )
    return pos
  } catch (err) {
    if (err.message === 'room-history-exceeded') throw err
    else if (err.message === 'room-expired') throw err
    else {
      debug(err)
      throw new Error('no-db-connection')
    }
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Get messages for specific room
 * @param {String} roomId - id of room
 * @param {Number} position - start position
 * @returns {Object} database result set
 */
async function getMessages (roomId, position) {
  let conn
  try {
    conn = await getConnection()
    const res = await conn.query('select * from messages where room = ? and id > ?', [roomId, position])
    return res
  } catch (err) {
    debug(err)
    throw new Error('no-db-connection')
  } finally {
    if (conn) conn.release()
  }
}
