/**
 * Installation script for the database, may only be started once and creates a first admin user
 * @author Thomas Kippenberg
 * @module install
 * @see INSTALL.md
 */

import mariadb from 'mariadb'
import config from './config.json' with { type: 'json' }
import { debug } from 'console'
import fs from 'fs'
import crypto from 'crypto'
import readlineSync from 'readline-sync'
import consts from './consts.js'
import process from 'process'

const pool = mariadb.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  connectionLimit: 5,
  multipleStatements: true
})

async function main () {
  const userId = readlineSync.question('User name of admin: ')
  if (!userId.match(consts.USER_ID_REGEX)) {
    console.log('Invalid Userid:\n- Only letters and digits\n-  At least 4 characters in length, but no more than 64.')
  }
  const password1 = readlineSync.question('Password of admin: ', { hideEchoBack: true })
  if (!password1.match(consts.PASSWORD_REGEX)) {
    console.log('Password to weak:\n- At least one digit [0-9]\n- At least one lowercase character [a-z]\n- At least one uppercase character [A-Z]\n- At least 8 characters in length, but no more than 32.')
    process.exit()
  }
  const password2 = readlineSync.question('Reenter password: ', { hideEchoBack: true })
  if (password1 !== password2) {
    console.log('Passwords don\'t match')
    process.exit()
  }
  let conn
  try {
    conn = await pool.getConnection({ multipleStatements: true })
    // run sql install script - WILL DELETE ALL TABLES!
    const sql = fs.readFileSync('./install.sql').toString()
    await conn.query(sql)
    // create initial account
    const hash = crypto.createHash('sha256').update(userId + password1).digest('hex')
    await conn.query('insert into users (id, password, locked, admin, description) values (?, ?, ?, ?, ?)', [userId, hash, false, true, 'Initial admin account'])
  } catch (err) {
    debug(err.message)
  } finally {
    conn.release()
  }
}

await main()
process.exit()
