import pluginJs from '@eslint/js'
import { FlatCompat } from '@eslint/eslintrc'
const eslintrc = new FlatCompat()

export default [
  ...eslintrc.extends('standard'),
  {
    files: ['**/*.js'],
    languageOptions: { sourceType: 'module' }
  },
  pluginJs.configs.recommended
]
