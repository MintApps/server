# MintApps - Node.js Server component

This repo provides this server component (based on Node.js), see [INSTALL.md](INSTALL.md) for installation instructions.

## Purpose

The MintApps contain an **optional** server component which serves the following purposes:

1. Synchronization between multiple guests in cooperative apps, such as quiz games
2. Saving your own activities, such as self-created quiz games

In other words, this means that the server component is only required by very few, special apps.

## Architecture

The optional MintApps server is based on nodejs and uses the Express framework. WebSockets are used to map real-time synchronization and mariadb serves as database.

The **Modules** area contains all central libraries, e.g. for accessing the database, validation or running the websocket-server.

## License

MintApps is licensed under the “[GNU General Public License 3](https://www.gnu.org/licenses/gpl-3.0.html)”, please note that this excludes any liability or warranty.

**Disclaimer of Warranty.**

There is no warranty for the program, to the extent permitted by applicable law. Except when otherwise stated in writing the copyright holders and/or other parties provide the program “as is” without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with you. Should the program prove defective, you assume the cost of all necessary servicing, repair or correction.

**Limitation of Liability**

In no event unless required by applicable law or agreed to in writing will any copyright holder, or any other party who modifies and/or conveys the program as permitted above, be liable to you for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by you or third parties or a failure of the program to operate with any other programs), even if such holder or other party has been advised of the possibility of such damages.