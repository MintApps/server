# Node Server installation

## Room protection

To prevent your sync server from being overloaded with requests, inviting additional participants is limited. There are three options available:

- password: use password authentication (login in the navigation menu)
- captcha: use captcha
- challenge: use proof of work challenge (barrier-free)

First, decide which method you would like to use. Your choice has an effect on the configuration of the server component.

## Prerequisites

- Only use this server if you really know how to install and operate web servers, databases and Node.js.
- Install a current version of Node.js and MariaDB on your server.
- Install and configure a reverse proxy that supports web sockets (such as nginx).
- Secure all installations and your operating system.

## Installation steps

- Create a database for the synchronization data in MariaDB (usually called "mintapps").
  ~~~
  create database mintapps;
  ~~~

- Create a database user using a strong password and grant full access to the database "mintapps".
  ~~~
  create user mintuser@localhost identified by 'enter-strong-password';
  grant all privileges on mintapps.* to mintuser@localhost;
  ~~~

- Download the latest [ZIP package](https://codeberg.org/MintApps/server-node/archive/main.zip) and unpack it
  ~~~
  wget https://codeberg.org/MintApps/server-node/archive/main.zip
  unzip main.zip
  rm main.zip
  ~~~

- install npm packages
  ~~~
  cd server-node
  npm install
  ~~~

- Copy the file `config.example.json` to `config.json`
  ~~~
  cp config.example.json config.json
  ~~~

- Edit the file `config.json` with your favorite editor.
- Adjust the database parameters
- Specify a long random string for `room.secret`
- If necessary, adjust the `server.cors` setting
- Set `room.auth` accordingly (see section "Room Protection")
- Check the other keys

  key | description | example
  --- | --- | ---
  db.host | hostname of mariadb server, change according to installation | "localhost"
  db.user | username for mariadb server | "mintuser"
  db.password | password for maridab server | "mintpass"
  db.database | databasename for mintapps db | "mintapps"
  server.port | port, the server is listening on; don't expose directly, use reverse proxy | 3000
  server.cors | optional list of allowed CORS Urls | ["https://your-server.de", "https://your-other-server.de"]
  server.provider | provider text for OTP tokens | your-instance-name"
  room.auth | see section "Room Protection | "challenge"
  room.secret | Specify a long random string | "insert-long-secret-here"
  room.limit | maximum number of sync rooms | 1000
  room.memberLimit | maximum number of members per sync room | 100
  room.historyLimit | maximum length of room history | 10000
  room.lifetime | lifetime of sync rooms in ms | 3600E3
  captcha.size | number of characters | 6
  captcha.lifetime | captcha will expire after <lifetime> milliseconds (should be less than one minute) | 30E3
  challenge.saltLength | length of the salt inside the challenge | 10
  challenge.strength | required number of trailing zeros of the hash value (maximum 6, see sync-challenge-worker.js) | 4
  challenge.lifetime | expire after <lifetime> milliseconds (should be at least one minute) | 300E3,
  challenge.limit | maximum number of challenges stored in the database | 1000
  activity.maxSize | maximum size for activities in MB | 20
  activity.quota | user quota for activities in MB | 100
  session.lifetime | lifetime of authenticated user in ms, default one hour | 3600E3
  session.cookie | cookie configuration for authenticated user | { "expires": 0, "sameSite": "none", "httpOnly": false, "secure": false }

- Run setup script and create an initial admin account (caution - this will fail if the tables already exist!)
  ~~~
  node install.js
  ~~~

- Start server by running `npm run start`, it will listen to port `server.port`
- Test the functionality of the server by calling the URL `get-captcha` (full path depends on your reverse proxy configuration). If the server works correctly, your browser will display an answer starting like
  ~~~
  captcha "data:image/svg+xml;base64,...
  ~~~

- Adjust the client's configuration as described in [Client/INSTALL.md](https://codeberg.org/MintApps/client/src/branch/main/INSTALL.md)
  ~~~
  {
    ...
    "syncUrl": "https://www.my-own-server.xxx/path"
  }
  ~~~
  You'll have to adjust "path" depending on your reverse proxy configuration

## Documentation

The server's source code is commented using jsdoc. Run the following command to generate HTML pages with the documentation:

~~~
npm run doc
~~~

You will then find the pages in the "doc" subdirectory