/**
 * Functions for creating and managing users of the MintApps
 * @author Thomas Kippenberg
 * @module server-users
*/

import * as helpers from './helpers.js'
import { getConnection } from './database.js'
import consts from './consts.js'
import crypto from 'crypto'
import config from './config.json' with { type: 'json' }
import * as OTPAuth from 'otpauth'
import { debug } from 'console'

/**
 * Create session (using username and password)
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function getSession (req, res) {
  let conn
  try {
    const data = req.body
    conn = await getConnection() // create session
    const now = Date.now()
    // validate userid
    if (!data.user || !data.user.match(consts.USER_ID_REGEX)) throw new Error('secret-invalid')
    // validate secret
    if (!data.secret) throw new Error('secret-invalid')
    // search entry in accounts
    const rs = await conn.query('select id, password, locked, admin, lastlogin, totp, totpsecret, lastaccess from users where id = ?', [data.user])
    if (rs.length < 1) throw new Error('secret-invalid')
    const user = rs[0]
    // user locked
    if (user.locked === 1) throw new Error('account-locked')
    // block fast attempts
    if (Number(user.lastaccess) + 5000 > now) {
      await conn.query('update users set lastaccess = ? where id = ?', [now, user.id])
      throw new Error('user-login-wait')
    }
    // check password
    const valid = user && user.password === crypto.createHash('sha256').update(data.user + data.secret).digest('hex')
    if (!valid) {
      await conn.query('update users set lastaccess = ? where id = ?', [now, user.id])
      throw new Error('secret-invalid')
    }
    // check topt
    if (user.totp) {
      if (!data.token) throw new Error('totp-missing') // no error, used in frontend
      await conn.query('update users set lastaccess = ? where id = ?', [now, user.id])
      if (!data.token.match(consts.USER_TOTP_REGEX)) throw new Error('totp-invalid')
      const totp = createToptToken(data.user, user.totpsecret)
      if (totp.validate({ token: data.token, window: 1 }) !== 0) throw new Error('totp-invalid')
    }
    // create session
    const session = helpers.getRandomToken(64)
    await conn.query('insert into sessions values (?, ?, ?, ?)', [session, user.id, now, now])
    // update lastlogin in db
    await conn.query('update users set lastlogin = ? where id = ?', [now, user.id])
    debug(`user ${user.id} login`)
    res.cookie('mintapps-session', session, config.session.cookie)
    res.json({ created: now, admin: user.admin === 1, totp: user.totp === 1, lastlogin: Number(user.lastlogin) })
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Check if session is valid and increase session timeout
 * @param {Request} req - request callback (not used)
 * @param {Response} res - response callback
 */
// eslint-disable-next-line no-unused-vars
export function updateSession (req, res) {
  res.json({})
}

/**
 * Delete session (using username and session)
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function deleteSession (req, res) {
  let conn
  try {
    const data = req.body
    conn = await getConnection()
    await conn.query('delete from sessions where user = ?', [data.user])
    res.json({})
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * ensure that request contains an valid session, middleware
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 * @param {Object} next - next callback
 */
export async function ensureValidSession (req, res, next) {
  if (await checkSession(req)) next()
  else res.json({ error: 'session-invalid' })
}

/**
 * Check if request contains valid session
 */
export async function checkSession (req) {
  const data = req.body
  let conn
  let valid = false
  try {
    let id = req.cookies['mintapps-session']
    if (!id) id = data.secret // TODO: remove after migration to mintapps 2.0
    // validate username and secret
    if (data.user && typeof data.user === 'string' && data.user.match(consts.USER_ID_REGEX) && id && typeof id === 'string' && id.match(consts.SESSION_REGEX)) {
      // check secret and time
      conn = await getConnection()
      const session = await conn.query('select * from sessions where id = ?', [id])
      const now = Date.now()
      valid = session.length === 1 && session[0].user === data.user && Number(session[0].changetime) + config.session.lifetime >= now
      // update timestamp in database
      if (valid) await conn.query('update sessions set changetime = ? where id = ?', [now, id])
    }
    return valid
  } catch (err) {
    debug(err)
    return false
  } finally {
    if (conn) conn.release()
  }
}

/**
 * change password as user
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function changePassword (req, res) {
  let conn
  try {
    const data = req.body
    const id = data.user
    conn = await getConnection()
    // validate new password
    const newpassword = helpers.getTextParam(data.newpassword, consts.PASSWORD_REGEX)
    if (!newpassword) throw new Error('password-invalid')
    const newhash = crypto.createHash('sha256').update(id + newpassword).digest('hex')
    // validate old password
    if (!data.password) throw new Error('secret-invalid')
    const oldhash = crypto.createHash('sha256').update(id + data.password).digest('hex')
    const rs = await conn.query('select id from users where id = ? and password = ?', [id, oldhash])
    if (rs.length < 1) throw new Error('secret-invalid')
    // update password
    await conn.query('update users set password = ? where id = ?', [newhash, id])
    debug(`user ${id} changed his password`)
    res.json({})
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Delete own user (providing password)
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function deleteUserWithPassword (req, res) {
  let conn
  try {
    const data = req.body
    const id = data.user
    conn = await getConnection()
    // validate password
    if (!data.password) throw new Error('secret-invalid')
    const hash = crypto.createHash('sha256').update(id + data.password).digest('hex')
    const rs = await conn.query('select id from users where id = ? and password = ?', [id, hash])
    if (rs.length < 1) throw new Error('secret-invalid')
    // delete user from db
    await conn.query('delete from users where id = ?', [id])
    // delete activities from db
    await conn.query('delete from activities where owner = ?', [id])
    // delete sessions from db
    await conn.query('delete from sessions where user = ?', [id])
    debug(`user ${id} deleted himself`)
    res.json({})
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Create totp token for user and return url for generating a qrcode
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function initTotp (req, res) {
  const data = req.body
  let conn
  try {
    conn = await getConnection()
    // exit if totp has been already enabled
    const rs = await conn.query('select * from users where totp = true && id = ?', [data.user])
    if (rs.length > 0) throw new Error('2fa-already-enabled')
    // create and store secret
    const secret = new OTPAuth.Secret()
    await conn.query('update users set totpsecret = ? where id = ?', [secret.base32, data.user])
    // create token and return url
    const totp = createToptToken(data.user, secret)
    res.json({ url: totp.toString() })
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Enable totp for user by validating given totp token
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function enableTotp (req, res) {
  const data = req.body
  let conn
  try {
    conn = await getConnection()
    if (!await checkTopt(req, false)) throw new Error('totp-invalid')
    await conn.query('update users set totp = true where id = ?', [data.user])
    res.json({})
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Disable totp for user by validating given totp token
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function disableTotp (req, res) {
  const data = req.body
  let conn
  try {
    conn = await getConnection()
    if (!await checkTopt(req)) throw new Error('totp-invalid')
    await conn.query('update users set totp = false where id = ?', [data.user])
    res.json({})
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Check, if reuqest contains a valid topt
 * @param {Request} req - request callback
 * @param {Boolean} testToptFlag - wether to test for topt flag in db
 */
async function checkTopt (req, testToptFlag = true) {
  const data = req.body
  let conn
  try {
    conn = await getConnection()
    if (!data.user || !data.user.match(consts.USER_ID_REGEX) || !data.token || !data.token.match(consts.USER_TOTP_REGEX)) return false
    const rs = await conn.query('select * from users where id = ?', [data.user])
    if (rs.length !== 1) return false // user not found
    if (testToptFlag && !rs[0].totp) return false // user no topt enabled
    const totp = createToptToken(data.user, rs[0].totpsecret)
    return totp.validate({ token: data.token, window: 1 }) === 0
  } catch (err) {
    debug(err)
    return false
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Create totp token
 * @param {String} secret - secret value
 * @param {String} user - user id
 */
function createToptToken (user, secret) {
  return new OTPAuth.TOTP({
    issuer: config.server.provider,
    label: user,
    algorithm: 'SHA256',
    digits: 6,
    period: 60,
    secret
  })
}

/* Admin specific methods */

/**
 * Ensure that request origins from admin user
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 * @param {Object} next - next callback
 */
export async function ensureIsAdmin (req, res, next) {
  if (await checkAdmin(req)) next()
  else res.json({ error: 'access-denied' })
}

/**
 * Check if request origins from admin user
 * @param {Request} req - request callback (not us)
 */
export async function checkAdmin (req) {
  let conn
  try {
    const data = req.body
    conn = await getConnection()
    const user = await conn.query('select * from users where admin = true && id = ?', [data.user])
    const admin = user.length === 1
    return admin
  } catch (err) {
    debug(err)
    return false
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Add new user or change existing user as admin
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function saveUser (req, res) {
  let conn
  try {
    const data = req.body
    conn = await getConnection()
    // don't allow user to change himself (to prevent from completely locking out...)
    if (data.user === data.id) throw new Error('access-denied')
    // validate new user id
    const id = helpers.getTextParam(data.id, consts.USER_ID_REGEX)
    if (!id) throw new Error('user-invalid')
    // validate description
    const description = helpers.getTextParam(data.description, consts.USER_DESCRIPTION_REGEX)
    // validate params
    const admin = data.admin === 'true'
    const locked = data.locked === 'true'
    // validate and hash new password (optional for existing user, mandatory for new user)
    let hash = false
    if (data.password) {
      const password = helpers.getTextParam(data.password, consts.PASSWORD_REGEX)
      if (!password) throw new Error('password-invalid')
      hash = crypto.createHash('sha256').update(id + password).digest('hex')
    }
    // check, if user already exists
    const rs = await conn.query('select id from users where id = ?', [data.id])
    const userExists = rs.length > 0
    // add user
    if (data.action === 'add') {
      if (userExists) throw new Error('user-already-exists')
      if (!hash) throw new Error('password-invalid') // password mandatory for new user
      await conn.query('insert into users (id, password, locked, admin, description) values (?, ?, ?, ?, ?)', [id, hash, false, false, description])
      debug(`user ${id} created`)
    } else if (data.action === 'update') {
      if (!userExists) throw new Error('user-doesnt-exist')
      if (!hash) await conn.query('update users set locked = ?, admin = ?, description = ? where id = ?', [locked, admin, description, id])
      else await conn.query('update users set password = ?, locked = ?, admin = ?, totp = false, description = ? where id = ?', [hash, locked, admin, description, id])
      debug(`user ${id} updated`)
    } else {
      throw new Error('invalid-action')
    }
    // cancel all sessions of the user (to avoid privileg leaks)
    await conn.query('delete from sessions where user = ?', [id])
    res.json({})
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Delete specific user as admin
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function deleteUserAsAdmin (req, res) {
  let conn
  try {
    const data = req.body
    conn = await getConnection()
    // validate user id to delete
    const id = helpers.getTextParam(req.body.id, consts.USER_ID_REGEX)
    if (!id || id === data.user) throw new Error('access-denied')
    // delete user from db
    await conn.query('delete from users where id = ?', [id])
    // delete activities from db
    await conn.query('delete from activities where owner = ?', [id])
    // delete sessions from db
    await conn.query('delete from sessions where user = ?', [id])
    debug(`admin deleted user ${id}`)
    res.json({})
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Get list of all users as admin
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
// eslint-disable-next-line no-unused-vars
export async function getUserList (req, res) {
  let conn
  try {
    conn = await getConnection()
    // get list from dn
    const list = await conn.query('select id, description, admin, locked, totp, lastlogin from users')
    list.forEach(element => {
      element.lastlogin = Number(element.lastlogin)
      element.admin = element.admin === 1
      element.locked = element.locked === 1
      element.totp = element.totp === 1
    })
    res.json(list)
    if (conn) conn.release()
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}
