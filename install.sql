/*
drop table if exists activities;
drop table if exists captchas;
drop table if exists challenges;
drop table if exists messages;
drop table if exists rooms;
drop table if exists users;
*/

/* Captchas used when creating rooms */
create table captchas (
  id int auto_increment,
  code varchar(32) not null,
  time bigint not null,
  primary key(id)
);

/* Challenges, also used when creating rooms */
create table challenges (
  id int auto_increment,
  challenge varchar(64) not null,
  time bigint not null,
  primary key (id)
);

/* Rooms used for synchronization */
create table rooms (
  id varchar(16) not null,
  name varchar(1000) not null,
  owner varchar(64) not null,
  createtime bigint not null,
  lifetime bigint not null,
  changetime bigint not null,
  memberlimit int not null,
  type varchar(100) not null,
  primary key (id)
);

/* Messages sent in the rooms */
create table messages (
  id int auto_increment,
  room varchar(16) not null,
  dst varchar(64) not null,
  src varchar(64) not null,
  time bigint not null,
  content mediumtext not null,
  primary key (id),
  foreign key (room) references rooms(id) on delete cascade
);

/* Activities */
create table activities (
  id varchar(64) not null,
  title varchar(300) not null,
  description varchar(500) not null,
  subject varchar(300),
  locale varchar(16),
  type varchar(16),
  author varchar(100),
  license varchar (100),
  icon mediumtext,
  owner varchar(64) not null,
  createtime bigint not null,
  data mediumtext not null,
  primary key (id)
);

/* User accounts */
create table users (
  id varchar(64) not null,
  description varchar(300),
  password varchar(64) not null,
  lastlogin bigint,
  lastaccess bigint,
  locked boolean default false,
  admin boolean default false,
  totp boolean default false,
  totpsecret varchar(64),
  primary key(id)
);

/* User sessions */
create table sessions (
  id varchar(64) not null,
  user varchar(64) not null,
  createtime bigint not null,
  changetime bigint not null,
  primary key(id)
)