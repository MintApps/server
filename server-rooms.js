/**
 * Functions for creating and managing rooms for synchronization in multiplayer games
 * @author Thomas Kippenberg
 * @module server-rooms
*/

import config from './config.json' with { type: 'json' }
import consts from './consts.js'
import * as helpers from './helpers.js'
import { getConnection } from './database.js'
import crypto from 'crypto'
import { debug } from 'console'
import * as serverWebsocket from './server-websocket.js'
import svgCaptcha from 'svg-captcha'
import * as serverUsers from './server-users.js'

/**
 * Get room authentification type (captcha, password, challenge)
 * @param {Request} req - request callback (not used)
 * @param {Response} res - response callback
 */
// eslint-disable-next-line no-unused-vars
export function getAuthType (req, res) {
  res.json({ type: config.room.auth })
}

/**
 * Get new captcha (as svg image)
 * @param {Request} req - request callback (not used)
 * @param {Response} res - response callback
 */
// eslint-disable-next-line no-unused-vars
export async function getCaptcha (req, res) {
  let conn
  const captcha = svgCaptcha.create({ size: config.captcha.size, ignoreChars: '1lo0O', noise: 3, color: true })
  try {
    conn = await getConnection()
    await conn.query('insert into captchas (code, time) values (?, ?)', [captcha.text, Date.now()])
    // eslint-disable-next-line no-undef
    const img = 'data:image/svg+xml;base64,' + Buffer.from(captcha.data).toString('base64')
    res.json({ captcha: img })
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Get cryptographic challenge (instead of captcha)
 * @param {Request} req - request callback (not used)
 * @param {Response} res - response callback
 */
// eslint-disable-next-line no-unused-vars
export async function getChallenge (req, res) {
  let conn
  try {
    const now = Date.now()
    const challenge = config.challenge.strength + ':' + helpers.getRandomToken(config.challenge.saltLength) + ':' + now
    conn = await getConnection()
    await conn.query('insert into challenges (challenge, time) values (?, ?)', [challenge, now])
    res.json({ challenge })
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Ensure that request origins from room owner, middleware
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 * @param {Object} next - next callback
 */
export async function ensureRoomOwner (req, res, next) {
  const data = req.body
  const user = helpers.getTextParam(data.user, consts.USER_ID_REGEX, false)
  const roomId = helpers.getTextParam(data.room, consts.ROOM_ID_REGEX, false)
  const token = helpers.getTextParam(data.token, consts.ROOM_TOKEN_REGEX, false)
  if (!user || !roomId || !token || !checkToken(roomId, user, token)) {
    return res.json({ error: 'token-invalid' })
  }
  let conn
  try {
    conn = await getConnection()
    const roomQuery = await conn.query('select * from rooms where id = ?', [roomId])
    if (roomQuery.length !== 1) return res.json({ error: 'room-expired' })
    else if (roomQuery[0].owner !== user) res.json({ error: 'not-owner' })
    else next()
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Create new room
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function createRoom (req, res) {
  let conn
  try {
    const data = req.body
    conn = await getConnection()
    const now = Date.now()
    let auth = false
    // check room number limit
    const rs = await conn.query('select count(id) as count from rooms')
    if (rs[0].count >= config.room.limit) throw new Error('room-max-number-exceeded')
    if (data.user) {
      // password-session (always possible, even when config says captcha or challenge)
      auth = await serverUsers.checkSession(req)
    } else if (config.room.auth === 'captcha' && data.secret && data.secret.match(consts.CAPTCHA_REGEX)) {
      // valid captcha ?
      const outDated = now - config.captcha.lifetime
      const res = await conn.query('delete from captchas where code = ? and time > ?', [data.secret, outDated])
      auth = res.affectedRows > 0
    } else if (config.room.auth === 'challenge' && data.secret.match(consts.CHALLENGE_REGEX)) {
      // valid challenge?
      const hash = crypto.createHash('sha256')
      const digest = hash.update(data.secret).digest('hex')
      const shouldBeZeros = '0'.repeat(config.challenge.strength)
      if (digest.endsWith(shouldBeZeros)) { // challenge not solved
        const parts = data.secret.split(':')
        const challenge = parts[0] + ':' + parts[1] + ':' + parts[2]
        const outDated = now - config.challenge.lifetime
        const res = await conn.query('delete from challenges where challenge = ? and time > ?', [challenge, outDated])
        auth = res.affectedRows > 0
      }
    }
    if (!auth) throw new Error('secret-invalid')
    // validate params
    const id = helpers.getRandomToken(12)
    const type = helpers.getTextParam(data.type, consts.ROOM_TYPE_REGEX, '')
    const name = helpers.getTextParam(data.name, consts.ROOM_NAME_REGEX, '')
    const memberLimit = helpers.getIntegerParam(data.memberLimit, 1, config.room.memberLimit, config.room.memberLimit)
    const lifetime = config.room.lifetime
    const owner = await serverUsers.checkSession(req) ? data.user : helpers.getRandomToken(12)
    const token = createToken(id, owner)
    // create room
    await conn.query(
      'insert into rooms (id, name, owner, createtime, lifetime, changetime, memberlimit, type) values (?, ?, ?, ?, ?, ?, ?, ?)',
      [id, name, owner, now, lifetime, now, memberLimit, type]
    )
    debug(`created new room ${id} with lifetime ${lifetime}ms`)
    res.json({ room: id, name, type, user: owner, owner, token, lifetime })
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Delete room
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function deleteRoom (req, res) {
  let conn
  try {
    conn = await getConnection()
    const data = req.body
    await conn.query('delete from rooms where id = ?', [data.room])
    // info to room all members of room
    serverWebsocket.sendMessageToRoom(data.room, JSON.stringify({ error: 'room-expired' }))
    debug(`deleted room ${data.room}`)
    res.json({})
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Set name information of room
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function setRoomName (req, res) {
  let conn
  try {
    const data = req.body
    // validate name
    const name = helpers.getTextParam(data.name, consts.ROOM_NAME_REGEX, '')
    // set name
    conn = await getConnection()
    await conn.query('update rooms set name = ? where id = ?', [name, data.room])
    debug(`update room name ${data.room} to ${name}`)
    res.json({})
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Get list of all rooms
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function getRoomList (req, res) {
  let conn
  try {
    const data = req.body
    const isAdmin = await serverUsers.checkAdmin(req)
    conn = await getConnection()
    // get list from db
    let list
    if (isAdmin) {
      list = await conn.query('select * from rooms order by createtime')
    } else {
      list = await conn.query('select * from rooms where owner = ? order by createtime', [data.user])
    }
    list.forEach(room => {
      room.lifetime = Number(room.lifetime)
      room.createtime = Number(room.createtime)
      room.changetime = Number(room.changetime)
      room.expires = room.lifetime + room.changetime
      room.token = createToken(room.id, room.owner)
    })
    res.json(list)
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Create user token for specific toom
 * @param {String} roomId - id of room
 * @param {String} user - id of user
 * @returns {String} - cryptographic token
 */
export function createToken (roomId, user) {
  const hash = crypto.createHash('sha256')
  const token = hash.update(roomId + user + config.room.secret).digest('hex')
  return token
}

/**
 * Check user and token for specific room
 * @param {String} roomId - id of room
 * @param {String} user - id of user
 * @param {String} token - token to check
 * @returns {Boolean} - wether token is valid
 */
export function checkToken (roomId, user, token) {
  if (!roomId || !user || !token) return false
  if (!token.match(consts.ROOM_TOKEN_REGEX)) return false
  if (token !== createToken(roomId, user)) return false
  return true
}
