/**
 * Central module of the synchronization server for the optional server component of MintApps
 * @author Thomas Kippenberg
 * @module server
 */

import https from 'https'
import http from 'http'
import express from 'express'
import helmet from 'helmet'
import fs from 'fs'
import config from './config.json' with { type: 'json' }
import { debug } from 'console'
import cors from 'cors'
import cookieParser from 'cookie-parser'
import { WebSocketServer } from 'ws'
import * as serverActivities from './server-activities.js'
import * as serverWebsocket from './server-websocket.js'
import * as serverRooms from './server-rooms.js'
import * as serverUsers from './server-users.js'

const app = express()
// set save headers
app.use(helmet())

// setup cors according to config
if (config.server.cors) {
  const options = {
    origin: config.server.cors,
    credentials: true
  }
  app.use(cors(options))
}

// add cookie parser
app.use(cookieParser())

// add parser for form data in post requests
app.use(express.urlencoded({
  extended: true,
  limit: `${config.activity.maxSize}mb`
}))

// create server
debug('Starting http server')
const server = http.createServer(app)

// room related methods
app.get('/get-auth-type', serverRooms.getAuthType)
app.get('/get-captcha', serverRooms.getCaptcha)
app.get('/get-challenge', serverRooms.getChallenge)
app.post('/create-room', serverRooms.createRoom)
app.post('/delete-room', serverRooms.ensureRoomOwner, serverRooms.deleteRoom)
app.post('/set-room-name', serverRooms.ensureRoomOwner, serverRooms.setRoomName)
app.post('/get-room-list', serverUsers.ensureValidSession, serverRooms.getRoomList)

// activity related methods
app.post('/get-activity', serverActivities.getActivity)
app.post('/get-activity-list', serverUsers.ensureValidSession, serverActivities.getActivityList)
app.post('/save-activity', serverUsers.ensureValidSession, serverActivities.saveActivity)
app.post('/delete-activity', serverUsers.ensureValidSession, serverActivities.deleteActivity)

// user related methods
app.post('/get-session', serverUsers.getSession)
app.post('/update-session', serverUsers.ensureValidSession, serverUsers.updateSession)
app.post('/delete-session', serverUsers.ensureValidSession, serverUsers.deleteSession)
app.post('/change-password', serverUsers.ensureValidSession, serverUsers.changePassword)
app.post('/delete-user-with-password', serverUsers.ensureValidSession, serverUsers.deleteUserWithPassword)
app.post('/init-totp', serverUsers.ensureValidSession, serverUsers.initTotp)
app.post('/enable-totp', serverUsers.ensureValidSession, serverUsers.enableTotp)
app.post('/disable-totp', serverUsers.ensureValidSession, serverUsers.disableTotp)
app.post('/get-user-list', serverUsers.ensureValidSession, serverUsers.ensureIsAdmin, serverUsers.getUserList)
app.post('/save-user', serverUsers.ensureValidSession, serverUsers.ensureIsAdmin, serverUsers.saveUser)
app.post('/delete-user-as-admin', serverUsers.ensureValidSession, serverUsers.ensureIsAdmin, serverUsers.deleteUserAsAdmin)

// Websocket server for syncing within rooms
const wss = new WebSocketServer({ server, path: '/socket' })
serverWebsocket.init(wss)

// start listening
server.listen(config.server.port)
