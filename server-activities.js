/**
 * Functions for storing and editing user data, which in the context of MintApps is referred to as "activity".
 * @author Thomas Kippenberg
 * @module server-activities
*/

import config from './config.json' with { type: 'json' }
import consts from './consts.js'
import * as helpers from './helpers.js'
import { getConnection } from './database.js'
import { getRandomToken } from './helpers.js'
import * as serverUsers from './server-users.js'

/**
 * Save an activity as a logged in user
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function saveActivity (req, res) {
  let conn
  try {
    const data = req.body
    const owner = data.user
    conn = await getConnection()
    // check quota
    const size = await conn.query('select sum(length(data)) as total from activities where owner = ?', [owner])
    if (size[0].total > config.activity.quota * 1E6) throw new Error('quota-exceeded')
    // validate params
    const activity = JSON.parse(data.activity)
    const title = helpers.getTextParam(activity.title, consts.ACTIVITY_TITLE_REGEX)
    const desc = helpers.getTextParam(activity.desc, consts.ACTIVITY_DESC_REGEX, '')
    const subject = helpers.getTextParam(activity.subject, consts.ACTIVITY_SUBJECT_REGEX, '')
    const locale = helpers.getTextParam(activity.locale, consts.ACTIVITY_LOCALE_REGEX, '')
    const type = helpers.getTextParam(activity.type, consts.ACTIVITY_TYPE_REGEX)
    const author = helpers.getTextParam(activity.author, consts.ACTIVITY_AUTHOR_REGEX, '')
    const license = helpers.getTextParam(activity.license, consts.ACTIVITY_LICENSE_REGEX, '')
    const icon = helpers.getTextParam(activity.icon, consts.ACTIVITY_ICON_INDEX, '')
    const overwrite = data.overwrite === 'true'
    if (title === null || type === null) throw new Error('activity-invalid')
    // clear duplicates to reduce storage size in db
    delete activity.id
    delete activity.title
    delete activity.desc
    delete activity.subject
    delete activity.locale
    delete activity.type
    delete activity.author
    delete activity.license
    delete activity.icon
    // create id and save
    const now = Date.now()
    // check if activity of same type, name and owner is already in db
    const rs = await conn.query('select id from activities where type = ? and owner = ? and title = ?', [type, owner, title])
    let id
    if (rs.length > 0) {
      id = rs[0].id // use existing id
      if (overwrite) await conn.query('delete from activities where id = ?', [id])
      else throw new Error('db-duplicate-entry')
    } else {
      id = `.${getRandomToken(20)}` // generate new random id for activity, must start with a dot
    }
    await conn.query(
      'insert into activities (id, title, description, subject, locale, type, author, license, icon, owner, data, createtime) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
      [id, title, desc, subject, locale, type, author, license, icon, owner, JSON.stringify(activity), now]
    )
    res.json({ id })
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Get list of all activities as logged in user
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function getActivityList (req, res) {
  let conn
  try {
    const data = req.body
    const isAdmin = await serverUsers.checkAdmin(req)
    // validate type
    let type = helpers.getTextParam(data.type, consts.ACTIVITY_TYPE_REGEX)
    if (!type) type = '%'
    // get list from db
    conn = await getConnection()
    let list
    if (isAdmin && type === '%') {
      list = await conn.query('select id, title, description, subject, icon, type, createtime, owner from activities where type like ? order by createtime', type)
    } else {
      list = await conn.query('select id, title, description, subject, icon, type, createtime, owner from activities where owner = ? and type like ? order by createtime', [data.user, type])
    }
    list.forEach(entry => {
      entry.desc = entry.description
      entry.createtime = Number(entry.createtime)
      delete entry.description
    })
    res.json(list)
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Get specific activitiy by id
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function getActivity (req, res) {
  let conn
  try {
    // validate id
    const id = helpers.getTextParam(req.body.id, consts.ACTIVITY_ID_REGEX)
    if (!id) throw new Error('activity-not-found')
    // get activities from db
    conn = await getConnection()
    const result = await conn.query('select * from activities where id = ?', id)
    if (result.length !== 1) throw new Error('activity-not-found')
    // populate attributes
    const activity = JSON.parse(result[0].data)
    activity.id = result[0].id
    activity.title = result[0].title
    activity.desc = result[0].description
    activity.subject = result[0].subject
    activity.locale = result[0].locale
    activity.type = result[0].type
    activity.author = result[0].author
    activity.license = result[0].license
    activity.icon = result[0].icon
    res.json(activity)
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}

/**
 * Delete specific activity by id as logged in user
 * @param {Request} req - request callback
 * @param {Response} res - response callback
 */
export async function deleteActivity (req, res) {
  let conn
  try {
    const data = req.body
    // validate id
    const id = helpers.getTextParam(req.body.id, consts.ACTIVITY_ID_REGEX)
    if (!id) throw new Error('activity-not-found')
    // delete activity
    const isAdmin = await serverUsers.checkAdmin(req)
    conn = await getConnection()
    if (isAdmin) {
      await conn.query('delete from activities where id = ?', [id])
    } else {
      await conn.query('delete from activities where id = ? and owner = ?', [id, data.user])
    }
    res.json({})
  } catch (err) {
    res.json({ error: err.message })
  } finally {
    if (conn) conn.release()
  }
}
